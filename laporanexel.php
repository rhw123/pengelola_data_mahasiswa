<?php 

require 'proses.php';

$databaru = tampildata("SELECT * FROM mahasiswa");

header("Content-type:aplication/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Mahasiswa.xls");


?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">


    <h1 class = "ml-5 mt-3">Data Mahasiswa</h1>

    <table border="1" class = "table table-bordered ml-5 mt-3" >
        <thead>       
           
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Alamat</th>
              
            </tr>
            
        </thead>
        <tbody>
            <?php $no = 1 ;?>
            <?php foreach($databaru as $dta) : ?>
            <tr>    
                <td><?php echo $no++ ?></td>
                <td><?php echo $dta['nama']; ?></td>
                <td><?php echo $dta['jurusan']; ?></td>
                <td><?php echo $dta['alamat']; ?></td>
               
                
            </tr>
            <?php endforeach; ?>
          
        </tbody>
    </table>

   

<?php require "footer.php"; ?>