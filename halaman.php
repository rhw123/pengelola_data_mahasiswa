<?php 
session_start();

if (empty($_SESSION['user']) && !isset($_SESSION['user']))
{
    header("Location: login.php");
}


require "proses.php";

$databaru = tampildata("SELECT * FROM mahasiswa");


?>

<?php require "header.php"; ?>



    <h1 class = "ml-5 mt-3 heading" style="padding-left: 125px;">Data Mahasiswa</h1>

   <div class="container">
       <div class="row">
           <div class="col-sm-12 col-lg-10">
                <table class = "table table-striped ml-5 mt-3">
        <thead class="thead-dark">       
           
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Alamat</th>
                <th>Aksi</th>
            </tr>
            
        </thead>
        <tbody>
            <?php $no = 1 ;?>
            <?php foreach($databaru as $dta) : ?>
            <tr>    
                <td><?php echo $no++ ?></td>
                <td><?php echo $dta['nama']; ?></td>
                <td><?php echo $dta['jurusan']; ?></td>
                <td><?php echo $dta['alamat']; ?></td>
                <td>
                    <a href="editdata.php?id=<?php echo $dta['id']; ?>" class = "btn btn-success btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a href="hapusdata.php?id=<?php echo $dta['id']; ?>" class = "btn btn-danger btn-sm ml-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
                
            </tr>
            <?php endforeach; ?>
            <a href="tambahdata.php" class = "btn btn-primary mt-3 mb-3 ml-5 "><i class="fa fa-floppy-o pr-2" aria-hidden="true"></i>Tambah Data</a>
            <a href="laporan.php" class = "btn btn-success ml-2"><i class="fa fa-file-pdf-o pr-2" aria-hidden="true"></i>Downloads PDF</a>
            <a href="laporanexel.php" class = "btn btn-warning ml-2"><i class="fa fa-table pr-2" aria-hidden="true"></i>Downloads Excell</a>
        </tbody>
    </table>
           </div>
       </div>
   </div>
   

<?php require "footer.php"; ?>