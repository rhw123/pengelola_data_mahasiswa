<?php 


session_start();

if (empty($_SESSION['user']) && !isset($_SESSION['user']))
{
    header("Location: login.php");
}


if (isset($_POST['simpan'])) {
    require "proses.php";
    tambahdata();
}


?>


<?php require "header.php" ?>

<h1 class = "ml-5 mt-3">Isi Form Berikut Ini</h1>

<div class = "Container ml-5 mt-3">
    <div class = "row">
        <div class = "col-md-6">
            <form method = "POST" action = "">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name = "nama">
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" class="form-control" id="jurusan" name = "jurusan">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name = "alamat">
                </div>
                <button type="submit" class="btn btn-primary" name = "simpan"><i class="fa fa-floppy-o pr-2" aria-hidden="true"></i>Simpan</button>
                <a href="index.php" class = "btn btn-success"><i class="fa fa-arrow-left pr-2" aria-hidden="true"></i>Kembali</a>
            </form>
        </div>
    </div>
</div>














<?php require "footer.php"; ?>