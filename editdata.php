<?php 

session_start();

if (empty($_SESSION['user']) && !isset($_SESSION['user']))
{
    header("Location: login.php");
}

require "proses.php";
$id = $_GET['id'];
$result = tampildata("SELECT * FROM mahasiswa WHERE id = $id")[0];


if (isset($_POST['edit']))
{
   
    $nama = $_POST['nama'];
    $jurusan = $_POST['jurusan'];
    $alamat = $_POST['alamat'];
    $conn = koneksi();
    $sql = "UPDATE mahasiswa SET nama = '$nama', jurusan='$jurusan', alamat='$alamat' WHERE id = $id";
    $query = $conn->query($sql);
    if ($query === TRUE) 
    {
        echo "<script>alert('anda berhasil Update data')</script>";
        echo "<script>location='index.php'</script>";
    }else 
    {
        echo "<script>alert('anda gagal Update data')</script>";
        echo "<script>location='tambahdata.php'</script>";
    }
}

?>


<?php require "header.php" ?>

<h1 class = "ml-5">Halaman Tambah Data</h1>

<div class = "Container ml-5">
    <div class = "row">
        <div class = "col-md-6">
            <form method = "POST" action = "">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name = "nama" value = "<?php echo $result['nama']; ?>">
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" class="form-control" id="jurusan" name = "jurusan" value = "<?php echo $result['jurusan']; ?>">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name = "alamat" value = "<?php echo $result['alamat']; ?>">
                </div>
                <button type="submit" class="btn btn-primary btn-sm" name = "edit">Edit</button>
                <a href="index.php" class = "btn btn-success btn-sm">Kembali</a>
            </form>
        </div>
    </div>
</div>














<?php require "footer.php"; ?>