<?php 


 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Arimo&family=Teko:wght@300&display=swap" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Welcome</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark tuv">
            <div class="container">
            <a class="navbar-brand" href="#">Univeristas Izu Lokal</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                <a class="nav-link active" href="index.php">Mahasiswa <span class="sr-only">(current)</span></a>
                <a class="nav-link" href="tambahdata.php">Tambah Data</a>
                <a class="nav-link" href="datalaporanmahasiswa.php">Laporan Mahasiswa</a>
                <a class="nav-link" href="logout.php">Logout</a>
                </div>
            </div>
        </div>   
    </nav>  